const mongoose =require('mongoose')

const userSchema = new mongoose.Schema({
    firstName:{
        type:String,
        required: [true, "First name is required."]
    },
    lastName:{
        type: String,
        required: [true, 'Last name is required.']
    },
    email:{
        type: String,
        required: [true, 'Email is required.']
    },
    password:{
        type: String,
    },
    mobileNo:{
        type: String
    },
    loginType:{
        type: String,
        // required: [true, 'Login type is requred.']
    },
    categories: [{
        categoryName:{
            type: String,
            // required: [true, 'Category name is required.']
        },
        categoryType:{
            type: String,
        }
        // ,
        // status:{
        //     type: Boolean,
        //     default: true
        // }
    }],
    records:[{
        categoryType:{
            type: String
        },
        categoryName:{
            type: String,
            // required: [true, 'Category name is required.']
        },
        amount:{
            type: Number,
            // required: [ true,' Amount is required.']
        },
        description:{
            type: String,
            // required: [true, 'Description is required.']
        },
        status:{
            type: Boolean,
            default: true
        },
        createdOn: {
            type: Date,
            default: new Date()
        }

    }]

})

module.exports = mongoose.model('user', userSchema)