//1. create vbackend folder and initilaize npm -init
//2. install express nmpm intstall express
//3.install all needed modules
        // -nodemon
        // -cors 
        // -npm install mongoose
        // -npm install bcrypt
        // -npm install json
        // -npm Install jsonwebtoken
        // npm install dotenv
        //npm install bcryptjs
//4. Ceate index file

        const express = require('express');
        const app = express();
        const mongoose = require('mongoose');
        const cors =require('cors');

        app.unsubscribe(cors());

        mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
        mongoose.connect('mongodb+srv://admin:admin@wdc028-course-booking.ux8nl.mongodb.net/budget_tracker?retryWrites=true&w=majority', {
                useNewUrlParser: true,
                useUnifiedTopology: true,
        useFindAndModify: false

        })

        app.use(express.json())
        app.use(express.urlencoded({ extended: true }))

        const userRoutes = require('./routes/user')

        app.listen(process.env.PORT || 4000, () =>{
        console.log(`API is online on PORT ${process.env.PORT || 4000}`)
        })
//5.  create folders controller, routes, models,  with each users.js file inside
//6. Populate model file 

        const mongoose =require('mongoose')

        const userSchema = new mongoose.Schema({
                firstName:{
                        type:String,
                        required: [true, "First name is required."]
                    },
                    lastName:{
                        type: String,
                        required: [true, 'Last name is required.']
                    },
                    email:{
                        type: String,
                        required: [true, 'Email is required.']
                    },
                    password:{
                        type: String,
                    },
                    mobileNo:{
                        type: String
                    },
                    loginType:{
                        type: String,
                        // required: [true, 'Login type is requred.']
                    },
                    categories: [{
                        categoryName:{
                            type: String,
                            // required: [true, 'Category name is required.']
                        },
                        categoryType:{
                            type: String,
                        }
                    }],
                    records:[{
                        categoryType:{
                            type: String
                        },
                        categoryName:{
                            type: String,
                            // required: [true, 'Category name is required.']
                        },
                        amount:{
                            type: Number,
                            // required: [ true,' Amount is required.']
                        },
                        descrition:{
                            type: String,
                            // required: [true, 'Description is required.']
                        }
                
                    }]
        
        })

        module.export=mongoose.model('user', userSchema)


//7. create Controllers for register && Email Exists
        const User = require('../models/user')

        module.exports.emailExists = (params) =>{
                return User.find({email:params.email}).then(result =>{
                    return result.length > 0 ? true : false
                })
            }

        module.exports.register =(params) =>{
                let user = new User({
                firstName: params.firstName,
                lastName: params.lastName,
                email: params.email,
                mobileNo: params.mobileNo,
                password: bcrypt.hashSync(undefined, 10),//password: params.password, 
                })
        
                return user.save().then((user,err) =>{
                return (err) ? false : true
                })
        }

//8. Create routes for register && email-exists
        const express= require('express');
        const router = express.Router();


        const UserController = require('../controllers/user')

        router.post('/email-exists', (req, res) =>{
                UserController.emailExists(req.body).then(result => res.send(result))
            })

        router.post('/', (req, res) =>{
        UserController.register(req.body).then(result => res.send(result))
        })

        module.exports = router


-------------------------------------------------------------

// Try putting bcrypt
// Create auth.js file
//9.
                const jwt = require('jsonwebtoken')
                const secret = 'CourseBookingAPI'

                module.exports.createAccessToken = (user) => {
                        const data = {
                                id: user._id,
                                email: user.email,
                        }

                        return jwt.sign(data, secret, {})
                }

                module.exports.verify = (req, res, next) => {
                        let token = req.headers.authorization

                        if (typeof token !== 'undefined') {
                                token = token.slice(7, token.length)

                                return jwt.verify(token, secret, (err, data) => {
                                        return (err) ? res.send({ auth: 'failed' }) : next()
                                })
                        } else {
                                return res.send({ auth: 'failed' })
                        }
                }

                module.exports.decode = (token) => {
                        if (typeof token !== 'undefined') {
                                token = token.slice(7, token.length)

                                return jwt.verify(token, secret, (err, data) => {
                                        return (err) ? null : jwt.decode(token, { complete: true }).payload
                                })
                        } else {
                                return null
                        }
                }

//10. Create login that generates access token, this must be the first ste so that we can add categories and recors to specific user

               //routes
               //login working
                router.post('/login', (req,res) =>{
                        UserController.login(req.body).then(result => res.send(result))
                }) 
                //login testing returning access token 
                //working
                module.exports.login =(params) =>{
                        return User.findOne({email:params.email})
                        .then(user =>{
                        if (user === null){
                                return { error: 'does-not-exist'}
                        }
                        if (user.loginType !== 'email'){
                            return { error: 'login-type-error'}
                        }
                        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
                                if(isPasswordMatched){
                                        return { accessToken: auth.createAccessToken(user.toObject()) }
                                }else{ 
                                        return { error: 'incorrect-password'}
                                }
                        })
                }


 //11 edit schema for categories and records, put status for deleiying and editing
                        status:{
                                type: Boolean,
                                default: true
                        }
//12 trying to work in controller for delteing still not working

//13Create react app for front end 
        //npx create-react-app frontend
        //npm install react-bootstrap bootstrap
        //npm install react-google-login
        //npm install sweetalert2 --save
        //npm install sweetalert2
        //npm install google-auth-library
//14 Create component folder
//15 In index.js kick start remove all unneccesarry contents
    // inside pages folder, create Register Folder with index inside
//16 In pages register > index.js Create your form page, Import form button and container 

import { Form, Button } from 'react-bootstrap';
import { Container } from 'react-bootstrap';

export default function index() {

   
   
    return(

        <Container >
            <h1 className="text-center">Register</h1>
            <Form  className="col-lg-4 offset-lg-4 my-5">

                <Form.Group>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="First Name"
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Last Name"
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter email"
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>


                <Form.Group>
                    <Form.Label>Mobile No.</Form.Label>
                    <Form.Control 
                        type="number"
                        placeholder="Mobile No."
                        required
                    />
                </Form.Group>


                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Password"
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Confirm Password"
                        required
                    />
                </Form.Group>

                <Button 
                    className="bg-primary"
                    type="submit"
                    id="submitBtn"
                >
                    Register
                </Button>
            
            </Form>
        </Container> 

    )
};           

//17 Code for registering
//working

import { useEffect, useState } from 'react';
import Router from 'next/router';

//bootstrap
import { Container } from 'react-bootstrap';
import { Form, Button } from 'react-bootstrap';

export default function index() {

    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState(0);
    const [isActive, setIsActive] =useState(false)

    function register(event){
        event.preventDefault();

        // fetch in the database to check on existing email
        fetch('http://localhost:4000/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body:JSON.stringify({
				email:email
			})
		})

        .then(res => res.json())
        .then(data =>{
            console.log(data)

            //if no existing
            if(data === false){
                fetch('http://localhost:4000/api/users',{
                    method: 'POST',
                    headers:{
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1 
                    })
                })
                .then(res => res.json())
                .then(data =>{
                    if(data){
                        setEmail('');
						setFirstName('');
						setLastName('');
						setMobileNo(0);
						setPassword1('');
						setPassword2('');

						alert('Thank you for registering with us!')
						Router.push('/login');
                    }
                })

            }else{
                alert('email exists')
            }

        })



    }


    useEffect(() =>{
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    },[email,password1,password2])
   
    return(

        <Container >
            <h1 className="text-center">Register</h1>
            <Form onSubmit={register} className="col-lg-4 offset-lg-4 my-5">

                <Form.Group>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="First Name"
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Last Name"
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>


                <Form.Group>
                    <Form.Label>Mobile No.</Form.Label>
                    <Form.Control 
                        type="number"
                        placeholder="Mobile No."
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>


                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Password"
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Confirm Password"
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>

                {isActive ?
                    <Button 
                        className="bg-primary"
                        type="submit"
                        id="submitBtn"
                    >
                        Register
                    </Button>
                    :
                    <Button 
                        className="bg-danger"
                        type="submit"
                        id="submitBtn"
                        disabled
                    >
                        Register
                    </Button>
                }
               
            </Form>
        </Container> 

    )
};

//18 Create file 
    //------ > UserContext
    import React from 'react';

    //create a Context Object
    const UserContext = React.createContext();

    //Provider component allows consuming components to subscribe to context changes
    export const UserProvider = UserContext.Provider;

    export default UserContext;

    //------->.env.local
    NEXT_PUBLIC_API_URL=http://localhost:4000/api

    //------->app-helper.js
    module.exports = {
        API_URL: process.env.NEXT_PUBLIC_API_URL,
        getAccessToken: () => localStorage.getItem('token'),
        toJSON:(response) => response.json()
    }

//18. Inside Ccomponents create View.js

        import React from 'react';
        import Head from 'next/head';
        import { Container } from 'react-bootstrap';

        const View = ({ title, children }) => {
            return (
                <React.Fragment>
                    <Head>
                        <title key="title-tag">{ title }</title>
                        <meta key="title-meat" name="viewport" content="initila-scale=1.0, width=device-width" />
                    </Head>
                    <Container className="mt-5 pt-4 mb-5">
                        { children }
                    </Container>
                </React.Fragment>
            )
        }

export default View;
//children parameter is a reserved keyword that contains the child/ sub component of another component  


//19 create routes and controllers for user details
        //routes
        router.get('/details', auth.verify, (req, res) => {
            const user = auth.decode(req.headers.authorization)
            UserController.get({ userId: user.id }).then(user => res.send(user))
        })

        //controllers
        module.exports.get = (params) => {
            return User.findById(params.userId).then(user => {
                user.password = undefined
                return user
            })
        }


//20. Inside pages folder, create login folder with index.js inside(note this part you will have an erro if the user provider in the _appjs doest poes a value to the components,)

import { useSate, useContext, useEffect, useState } from 'react';
import Router from 'next/router';
import { Form, Button, Card , Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import View from '../../components/View';

export default function index() {

    return(  
        <View title={'Login'}>
            <Row className="justify-content-center">	
                <Col xs md="6">	
                    <h3>Login</h3>
                    <LoginForm />
                </Col>
            </Row>
        </View>
    )

}

const LoginForm = () => {

    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');;
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] =useState(false);

    function authenticate(e){
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }


        fetch(`${AppHelper.API_URL}/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data =>{
            //data is the object with key value of accessToken
            // console.log(data);

            if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)

                  //redirect to records pages
                  Router.push('/records')
            }else{
                if(data.error === 'does-not-exist'){
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password'){
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error'){
                    Swal.fire('Login Type Error', 'You may have registered throug a different login procedure, try alternative login procedure.', 'error')
                }
            }

        })

    };


    const retrieveUserDetails = (accessToken) => {

        const options = {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data);
            setUser({
                id: data._id
            })
        })

    }

   

    return(

        <Card>
            <Card.Header>Login Details</Card.Header>
            <Card.Body>
                <Form onSubmit={ authenticate }>

                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password"
                            placeholder="Password" 
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Button className="bg-primary" type="submit">
                            Login
                    </Button>

                    <GoogleLogin 
                        buttonText = "Login Using Google"
                    />
                
                </Form>
            </Card.Body>
        </Card>

    )

}






//-------------------
//21 In _app.js maximize the use of userContext and pass all the needed details to  all components by wrapping all components inside the UserProvider to pass information

            import React, { useState, useEffect } from 'react';
            // Import global css
            import '../styles/globals.css'
            // Import CSS Bootstrap
            import 'bootstrap/dist/css/bootstrap.min.css';
            import { Container } from 'react-bootstrap';
            // Import Context Provider
            import { UserProvider } from '../UserContext';
            import AppHelper from '../app-helper';
            
            function MyApp({ Component, pageProps }) {
                // State hook for user state, define here for global scope 
                const [user, setUser] = useState({
                    // Initialized as an object with properties set as null
                    // Proper values will be obtained from localStorage AFTER component gets rendered due to Next.JS pre-rendering
                    id: null
                })

            console.log(user);

            // Function for clearing local storage upon logout
            const unsetUser = () => {
                localStorage.clear();
                // Set the user global scope in the context provider to have its email set to null
                setUser({
                    id: null
                });
            }

            // localStorage can only be accessed after this component has been rendered, hence the need for an effect hook
            useEffect(() => {
                const options = {
                    headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
                }

                fetch(`${ AppHelper.API_URL }/users/details`, options)
                .then(AppHelper.toJSON)
                .then((data) => {
                    if (typeof data._id !== 'undefined') {
                        setUser({ id: data._id})
                    } else {
                        setUser({ id: null})   
                    }
                })
            }, [user.id])

            // Effect hook for testing the setUser() functionality
            useEffect(() => {
                console.log(user.id);
            }, [user.id])

            return (
                <React.Fragment>
                {/* Wrap the component tree within the UserProvider context provider so that components will have access to the passed in values here */}
                <UserProvider value={{user, setUser, unsetUser}}>
                    <Container>
                        <Component {...pageProps}/>
                    </Container>
                </UserProvider>
                </React.Fragment>
            ) 
            }

            export default MyApp


//22. Inside components create file NavBar.js temporariyl for log out purpose

//23 In pages create logout

import { useContext, useEffect } from 'react';
import Router from 'next/router';
import UserContext from '../../UserContext';

export default function index() {

    // Consume the UserContext and destructure it to access the user and unsetUser values from the context provider
    const { unsetUser } = useContext(UserContext);
    
    // Invoke unsetUser only after initial render
    useEffect(() => {
        // Invoke unsetUser() to clear local storage of user info
        unsetUser();
        Router.push('/login');
    })

    return null;

}

-----------

//23 Create Google login 
    // first create a project in console.cloud.google.com -> copy the client Id needed for google-login 

    // add the google button in login form
    // create the google  authenticateGoogleToken function inside the LoginForm
    import { useSate, useContext, useEffect, useState } from 'react';
import Router from 'next/router';
import { Form, Button, Card , Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import View from '../../components/View';


export default function index() {

    return(  
        <View title={'Login'}>
            <Row className="justify-content-center">	
                <Col xs md="6">	
                    <h3>Login</h3>
                    <LoginForm />
                </Col>
            </Row>
        </View>
    )

}

const LoginForm = () => {

    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');;
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] =useState(false);

    function authenticate(e){
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }


        fetch(`${AppHelper.API_URL}/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data =>{
            //data is the object with key value of accessToken
            // console.log(data);

            if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)

                //redirect temporarily to index.js
                Router.push('/records')
            }else{
                if(data.error === 'does-not-exist'){
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password'){
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error'){
                    Swal.fire('Login Type Error', 'You may have registered throug a different login procedure, try alternative login procedure.', 'error')
                }
            }

        })

    };


    const authenticateGoogleToken = (response) => {
        console.log(response.tokenId)

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        }

        fetch(`${ AppHelper.API_URL }/users/verify-google-id-token`, options)
        .then(AppHelper.toJSON)
        .then(data => {

            console.log(data);

            if (typeof data.accessToken !== 'undefined'){

                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);

            } else {

                if (data.error === 'google-auth-error'){
                    Swal.fire(
                        'Google Auth Error', 
                        'Google authentication procedure failed.', 
                        'error'
                    )
                } else if (data.error === 'login-type-error') {
                    Swal.fire(
                        'Login Type Error', 
                        'You may have registered through a different login procedure.', 
                        'error'
                    )
                }

            }

        })

    };


    const retrieveUserDetails = (accessToken) => {

        const options = {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data);
            setUser({
                id: data._id
            })
        })

    }

    return(

        <Card>
            <Card.Header>Login Details</Card.Header>
            <Card.Body>
                <Form onSubmit={ authenticate }>

                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password"
                            placeholder="Password" 
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Button className="bg-primary" type="submit">
                            Login
                    </Button>

                    <GoogleLogin
                        clientId="10444938299-43406hruphuohmc78cqr4v6mlv01t4op.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />
                
                </Form>
            </Card.Body>
        </Card>
    )

}


//24 Create the routes and Contorller for Google login 
    //Note in backend need to install: npm install google-auth-library
    //google login routes

    router.post('/verify-google-id-token', async (req, res) => {
        res.send( await UserController.verifyGoogleTokenId(req.body.tokenId))
    })

    //google login controller 
    //include the clientId and  OAuth2client

    const { OAuth2Client } = require('google-auth-library');

    const User = require('../models/user')
    const bcrypt = require('bcryptjs');
    const auth = require('../auth')
    const clientId ='10444938299-43406hruphuohmc78cqr4v6mlv01t4op.apps.googleusercontent.com'

    module.exports.verifyGoogleTokenId = async (tokenId) => {

        const client = new OAuth2Client(clientId);
        const data = await client.verifyIdToken({
            idToken: tokenId,
            audience: clientId
        })
    
        console.log(data.payload.email_verified);
    
        if(data.payload.email_verified === true) {
    
            const user = await User.findOne({ email: data.payload.email });
    
            console.log(data.payload);
    
            if (user !== null) {
                
                if (user.loginType === "google") {
                    return { accessToken: auth.createAccessToken(user.toObject()) };
                } else {
                    return { error : "login-type-error" }
                }
    
            } else {
    
                let user = new User({
                    firstName: data.payload.given_name,
                    lastName: data.payload.family_name,
                    email: data.payload.email,
                    loginType: 'google'
                })
    
                return user.save().then((user, err) => {
                    return { accessToken: auth.createAccessToken(user.toObject()) };
                })
    
            }
    
        } else {
    
            return { error: "google-auth-error" }
    
        }
    
    };


//25 Add category page with index.js and new.js inside

    // new.js ------
        import { Form } from 'react-bootstrap'
        import {Button, Container, Card} from 'react-bootstrap'

        export default function index(){
            
            
            
            return(
            <Container className="mt-5 pt-4 mb-5 ">
                <div className="justify-content-center row">
                    <div className="col-md-6 col">
                            <h3>New Category</h3>
                            <Card>
                                <Card.Header>Category Information</Card.Header>
                                <Card.Body>
                                    <Form>
                                        <Form.Group>
                                            <Form.Label className="form-label" htmlFor="categoryName">Category Name:</Form.Label>
                                            <Form.Control
                                                type="text"
                                                placeholder="Enter category name"
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group>
                                            <Form.Label className="form-label" htmlFor="categoryType">Category Type:</Form.Label>
                                            <Form.Control as="select" placeholder="Enter category name" required>
                                                    <option vlaue="true" disabled>Select category</option>
                                                    <option value="Income">Income</option>
                                                    <option value="Expense">Expense</option>
                                            </Form.Control>
                                        </Form.Group>
                                        <Button className="btn btn-primary">Submit</Button>
                                    </Form>
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                    
                </Container> 
            )
        }

    
        //index.js to be populated with code later for functionalities
        import {Table,Button, Container} from 'react-bootstrap';
import Link from 'next/link';

export default function index(){
    
    return (
        <Container className="mt-5 pt-4 mb-5">
             <h3>Categories</h3>
             <Link href="/categories/new">
                <Button className="mt-1 mb-3">Add</Button>
             </Link>
             
             <Table className="table table-stripped table-bordered table-hover">
                 <thead>
                     <tr>
                         <th>Category</th>
                         <th>Type</th>
                     </tr>
                 </thead>
                 <tbody>
                     <tr>
                         <td>Test 1 Cat</td>
                         <td>Test 1 Type</td>
                     </tr>
                 </tbody>
             </Table>
        </Container>
       
    )
}

//26. I move the login code to component and call it in the index.js so that it will open during start

//27. Add records folder in pages inside records folder, I add index.js and new.js
    //I edited the Navbar as well by putting logout ot the left
  // index.js
import { Form } from 'react-bootstrap'
import FormControl from 'react-bootstrap/FormControl'
import {Button, Container, Card, InputGroup} from 'react-bootstrap'
 
 export default function Record(){
     return(
         <Container className="mt-5 mb-5 pt-4">
            <h3>Records</h3>
            <InputGroup>
                <InputGroup.Prepend>
                    <a className="btn btn-success" href="/records/new">Add</a>
                </InputGroup.Prepend>

                <FormControl placeholder="Search Record"/>
                <Form.Control as="select">
                    <option value="all">All</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </InputGroup>
            <Card className="mb-3">
                <Card.Body>
                    <div className="row">
                        <div className="col-6">
                            <h5>Sample E bills</h5>
                            <h6>Sample Expense(bills)</h6>
                            <p>Sample April 30,2021</p>
                        </div>
                        <div className="text-right col-6">
                            <h6>-300</h6>
                            <span>46200</span>

                        </div>
                    </div>

                </Card.Body>
            </Card>
           
         </Container>
     )
 }

        //new.js-------

import { Form } from 'react-bootstrap'
import {Button, Container, Card} from 'react-bootstrap'

export default function index(){
    
    return(
       <Container className="mt-5 pt-4 mb-5 ">
           <div className="justify-content-center row">
               <div className="col-md-6 col">
                    <h3>New Record</h3>
                    <Card>
                        <Card.Header>Record Information</Card.Header>
                        <Card.Body>
                            <Form>
                            <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryType">Category Type:</Form.Label>
                                    <Form.Control as="select" required>
                                            <option vlaue="true" disabled>Select category</option>
                                            <option value="Income">Income</option>
                                            <option value="Expense">Expense</option>
                                    </Form.Control>
                                </Form.Group>

                              
                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryType">Category Name:</Form.Label>
                                    <Form.Control as="select" htmlFor="categoryName" required>
                                            <option vlaue="true" disabled>Select category</option>
                                            <option></option>
                                    </Form.Control>
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="amount">Amount:</Form.Label>
                                    <Form.Control
                                        type="number"
                                        placeholder="Enter amount"
                                        required
                                    />
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="description">Description:</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter description"
                                        required
                                    />
                                </Form.Group>

                                <Button className="btn btn-primary">Submit</Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
            
        </Container> 
    )
}

//28. Create function for adding category

import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext'
import AppHelper from '../../app-helper'

import { Form } from 'react-bootstrap'
import {Button, Container, Card} from 'react-bootstrap'
import { Router } from 'next/router';

export default function index(){
    
    const { user } = useContext(UserContext);
 

    const [categoryName, setCategoryName] = useState('')
    const [categoryType, setCategoryType] = useState('')

    function addCategory(e){
        e.preventDefault()
        
        //ito para ma check ko kung naka login yung if may user.id pag true
        if(user.id){
          
            const options = {
                method: 'POST',
                headers: {
                     'Content-Type':'application/json',
                     Authorization: `Bearer ${ AppHelper.getAccessToken() }` 
                },
                body: JSON.stringify({
                    categoryType: categoryType,
                    categoryName: categoryName
                })
            }

            fetch(`${ AppHelper.API_URL }/users/categories`, options)
            .then(AppHelper.toJSON)
            .then(data =>{
                console.log(data)
                setCategoryName
            })
        }else{
            console.log("user id is null")
        }

    }
    
    return(
       <Container className="mt-5 pt-4 mb-5 ">
           <div className="justify-content-center row">
               <div className="col-md-6 col">
                    <h3>New Category</h3>
                    <Card>
                        <Card.Header>Category Information</Card.Header>
                        <Card.Body>
                            <Form onSubmit={addCategory}>
                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryName">Category Name:</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter category name"
                                        value={categoryName}
                                        onChange={e => setCategoryName(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryType">Category Type:</Form.Label>
                                    <Form.Control as="select" placeholder="Enter category name" required
                                        onChange={e => setCategoryType(e.target.value)}
                                    >
                                            <option vlaue="true" disabled>Select category</option>
                                            <option value="Income">Income</option>
                                            <option value="Expense">Expense</option>
                                    </Form.Control>
                                </Form.Group>
                                <Button className="btn btn-primary" type="submit">Submit</Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>  
        </Container> 
    )
}

//29. Add record function 

import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext'
import AppHelper from '../../app-helper'


import { Form } from 'react-bootstrap'
import {Button, Container, Card} from 'react-bootstrap'

import Swal from 'sweetalert2';

export default function index(){

    const { user } = useContext(UserContext);

    const [categories, setCategories] = useState([]);

    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const [amount, setAmount] = useState(0);
    const [description, setDescription] = useState('');


    function addRecord(e){
        e.preventDefault();



        fetch('http://localhost:4000/api/users/records', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ AppHelper.getAccessToken() }` 
            },
            body: JSON.stringify({
                categoryType: categoryType,
                categoryName: categoryName,
                amount: amount,
                description: description
            })
        })
            .then(res => res.json())
            .then(data =>{
                Swal.fire('New record added', `${categoryType} successfully added`, 'success') 
            })

    }



       //CODE TO DISPLAY THE CATEGORY NAME VALUES WITH THERE RESPECTED CATEGORY TYPE
       useEffect(() =>{
            console.log(user.id)

            fetch('http://localhost:4000/api/users/details', {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${ AppHelper.getAccessToken() }` 
                }
            })
            .then(res => res.json())
            .then(data =>{
                console.log(data.categories)
                console.log(categoryType)

                if(categoryType === "Expense"){

                    const categoryList = data.categories.map(category =>{
                      
                        if(category.categoryType === "Expense"){
                              console.log(category.categoryName)
                              console.log(category._id)
                            return(
                                <option key={category._id}>{category.categoryName}</option>
                            )
                        }
                    })
                    setCategories(categoryList)
                }

                if(categoryType === "Income"){

                    const categoryList = data.categories.map(element =>{
                        // console.log(element.categoryName)
                        if(element.categoryType === "Income"){
                            return(
                                <option key={element.categoryName}>{element.categoryName}</option>
                            )
                        }
                    })
                    setCategories(categoryList)
                }
                   
            })
       }, [categoryType])
        

    
    return(
       <Container className="mt-5 pt-4 mb-5 ">
           <div className="justify-content-center row">
               <div className="col-md-6 col">
                    <h3>New Record</h3>
                    <Card>
                        <Card.Header>Record Information</Card.Header>
                        <Card.Body>
                            <Form onSubmit={addRecord}>
                            <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryType">Category Type:</Form.Label>
                                    <Form.Control as="select" required onChange={e => setCategoryType(e.target.value)} >
                                            <option disabled>Select category</option>
                                            <option value="Income">Income</option>
                                            <option value="Expense">Expense</option>
                                    </Form.Control>
                                </Form.Group>

                              
                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryType">Category Name:</Form.Label>
                                    <Form.Control as="select" htmlFor="categoryName" onChange={e => setCategoryName(e.target.value)}>
                                           {categories}
                                    </Form.Control>
                                
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="amount">Amount:</Form.Label>
                                    <Form.Control
                                        type="number"
                                        placeholder="Enter amount"
                                        value={amount}
                                        onChange={e => setAmount(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="description">Description:</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter description"
                                        value={description}
                                        onChange={e => setDescription(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                             
                               
                                <Button className="btn btn-primary" type="submit">Submit</Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
            
        </Container> 
    )
}



//30 Add function Display to categories to display all categories in browser

//categoris index.js
import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext'
import AppHelper from '../../app-helper'



import {Table,Button, Container} from 'react-bootstrap';
import Link from 'next/link';

export default function index(){

    const { user } = useContext(UserContext);
    const [categoryList, setCategoryList] = useState([])

    useEffect(() =>{
        if(user.id !== null) {
            const categoryRows = user.categories.map(element =>{
                console.log(element.categoryName)
                console.log(element.categoryType)
                return(
                    <tr key={element._id}>
                        <td>{element.categoryName}</td>
                        <td>{element.categoryType}</td>
                    </tr>
                )
            });
            
            setCategoryList(categoryRows)
        }
    }, [categoryList])

    return (
        <Container className="mt-5 pt-4 mb-5">
             <h3>Categories</h3>
             <Link href="/categories/new">
                <Button className="mt-1 mb-3">Add</Button>
             </Link>
             
             <Table className="table table-stripped table-bordered table-hover">
                 <thead>
                     <tr>
                         <th>Category</th>
                         <th>Type</th>
                     </tr>
                 </thead>
                 <tbody>
                     {categoryList}
                 </tbody>
             </Table>
        </Container>
       
    )
}

// 31 Add records filtering in records index.js

import React, { useState, useEffect, useContext } from 'react';
import UserContext, { UserProvider } from '../../UserContext'
import Router from "next/router";


import { Form } from 'react-bootstrap'
import FormControl from 'react-bootstrap/FormControl'
import {Button, Container, Card, InputGroup} from 'react-bootstrap'

 export default function Record(){
  
    const { user } = useContext(UserContext);
    // console.log(user.records)
    const [searchResult, setSearchResult] = useState("");
    
    const [searchWord, setSearchWord] = useState("")

    const [searchType, setSearchType] = useState("All")

    const [bal, setBal] = useState(0);
    
    let countBalance = 0;

        // Router.reload()
        useEffect(() =>{
            if(user.id !== null){
                for (let record of user.records){

                    if(record.categoryType === "Income"){
                        countBalance = countBalance + record.amount 
                        
                    }else if( record.categoryType === "Expense"){
                        countBalance = countBalance - record.amount
                    }

                }
                setBal(countBalance)
            }
        },[bal])    

    
        useEffect(() =>{
            console.log(searchType)

            if(user.id !== null){
                //map through all records
                const searchDiv = user.records.map(record =>{
                    console.log(searchWord)
                        if(searchType === "All"){
                            if(record.description.toLowerCase().includes(searchWord)){
                                return(
                                    <React.Fragment>
                                        <div className="row" key={record._id}>
                                            <div className="col-6" key={record.description}>
                                                <h5>{record.description}</h5>
                                                <h6>{record.categoryType}</h6>
                                                <p>{record.createdOn}</p>
                                            </div>
                                            <div className="text-right col-6" key={`${record.amount} ${record._id}`}>
                                                <h6>{record.amount}</h6>
                                            </div>
                                        </div> 
                                    </React.Fragment>
                                   
                                )
                            }
                        }else if(searchType === "Income"){
                            if(record.categoryType === "Income"){
                                if(record.description.toLowerCase().includes(searchWord)){
                                    return(
                                        <React.Fragment>
                                            <div className="row" key={record._id}>
                                                <div className="col-6" key={record.description}>
                                                    <h5>{record.description}</h5>
                                                    <h6>{record.categoryType}</h6>
                                                    <p>{record.createdOn}</p>
                                                </div>
                                                <div className="text-right col-6" key={`${record.amount} ${record._id}`}>
                                                    <h6>{`+ ${record.amount}`}</h6>
                                                </div>
                                            </div> 
                                        </React.Fragment>
                                    )
                                }
                            }
                           
                        }else if( searchType === "Expense"){
                            if(record.categoryType === "Expense"){
                                if(record.description.toLowerCase().includes(searchWord)){
                                    return(
                                        <React.Fragment>
                                            <div className="row" key={record._id}>
                                                <div className="col-6" key={record.description}>
                                                    <h5>{record.description}</h5>
                                                    <h6>{record.categoryType}</h6>
                                                    <p>{record.createdOn}</p>
                                                </div>
                                                <div className="text-right col-6" key={`${record.amount} ${record._id}`}>
                                                    <h6>{`- ${record.amount}`}</h6>
                                                </div>
                                            </div> 
                                        </React.Fragment>
                                    )
                                }
                            }
                        }
                    
                })

                setSearchResult(searchDiv);
            }
           
        }, [searchWord, searchType])   

        console.log(bal)
        
   
        // useEffect(() =>{

        //     // for searching code
        //     if(user.id !== null){
        //         const searchDiv = user.records.map(record =>{
        //             console.log(searchWord)
        //             if(record.description.includes(searchWord)){
        //                 return(
        //                     <div key={record._id}>
                                    
        //                         <h5 key={record.description}>{record.description}</h5>
        //                         <h6 key={record.categoryType}>{record.categoryType}</h6>
        
        //                         <h6 key={record.amount}>{record.amount}</h6>
                            
        //                     </div>
        //                 )
        //             }
        //         })
        //         setSearchResult(searchDiv);
        //     }
           
        // }, [searchWord])   
    

     return(
         <Container className="mt-5 mb-5 pt-4">
            <h3>Records</h3>
                <InputGroup>
                    <InputGroup.Prepend>
                        <a className="btn btn-success" href="/records/new">Add</a>
                    </InputGroup.Prepend>

                    <FormControl placeholder="Search Record"
                        value={searchWord}
                        onChange={e => setSearchWord(e.target.value.toLowerCase())}
                    />
                    <Form.Control as="select" onChange={e => setSearchType(e.target.value)}>
                        <option value="All">All</option>
                        <option value="Income">Income</option>
                        <option value="Expense">Expense</option>
                    </Form.Control>
                </InputGroup>
                <Card className="mb-3">
                    <Card.Body>
                        {/* <div className="row">
                            <div className="col-6">
                                <h5>Sample E bills</h5>
                                <h6>Sample Expense(bills)</h6>
                                <p>Sample April 30,2021</p>
                            </div>
                            <div className="text-right col-6">
                                <h6>-300</h6>
                                <span>46200</span>
                            </div>
                        </div> */}
                        <div>
                            <h3>
                                Remaining Balance : {`${bal}`} 
                            </h3>
                        </div>
                        {searchResult}
                    </Card.Body>
                </Card>
         </Container>
     )
 }

 // 32. Create Charts isntall 
        // npm install react-chartjs-2
        // chart.js
        // npm install react-moment
        //npm install bootstrap react-bootstrap chart.js react-chartjs-2 moment
        // create a helper fucntion for color random generation ins sparate file names helpers

// code for breakdown piechart
        import React from 'react';
import {Pie} from 'react-chartjs-2';
import {useState, useEffect, useContext} from 'react';;
import UserContext from '../../UserContext';
import {Container, Form, NavItem} from 'react-bootstrap';
import moment from 'moment';

import { colorRandomizer, helloWorld } from '../../helpers';


export default function(){


    const { user } = useContext(UserContext);
    console.log(user.records)

    const [monthlyIncome, setMonthlyIncome] = useState([]);
    const [incomeName, setName] = useState([]);
    const [expenseName, setExpenseName] = useState([]);
    const [monthlyExpense, setMonthlyExpense] =  useState([]);
    const [dateStart, setDateStart] = useState("");
    const [dateEnd, setDateEnd] = useState("");
    const [bgColors, setBgColors] = useState([]);


    useEffect(() =>{
     
        if(dateStart !== "" && dateEnd !== ""){

            let income = [];
            let incomeLabel =[];
            let expense =[];
            let expenseLabel =[];

            user.records.forEach(record => {
                if((moment(record.createdOn).format("YYYY-MM-DD") >= dateStart) && (moment(record.createdOn).format("YYYY-MM-DD") <= dateEnd)){
                    if(record.categoryType === "Income"){
                        incomeLabel.push(record.categoryName)
                    }else if( record.categoryType === "Expense"){
                        expenseLabel.push(record.categoryName)
                    }
                }
            })
            setName(incomeLabel);
            setExpenseName(expenseLabel);
        

            user.records.forEach(record => {
                
                if((moment(record.createdOn).format("YYYY-MM-DD") >= dateStart) && (moment(record.createdOn).format("YYYY-MM-DD") <= dateEnd)){
                    if(record.categoryType === "Income"){
                        income.push(record.amount)
                    }else if(record.categoryType === "Expense"){
                        expense.push(record.amount)
                    }
                }
            })
            setMonthlyIncome(income)
            setMonthlyExpense(expense)

            setBgColors(user.records.map(() => `#${colorRandomizer()}`));

        }
    },[dateStart,dateEnd])


    console.log(incomeName)
    console.log(monthlyIncome)
   

    const dataIncome ={
        labels: incomeName,
        datasets: [{
            data: monthlyIncome,
            backgroundColor: bgColors
        }]
    }

    const dataExpense ={
        labels: expenseName,
        datasets: [{
            data: monthlyExpense,
            backgroundColor: bgColors
        }]
    }
    
    return(
        <Container className="mt-5 pt-4 mb-5">
            <h3>Income and Expense Category Breakdown</h3>
            <Form className="row">
                <Form.Group className="col-6">
                    <Form.Label>From</Form.Label>
                        <Form.Control
                            type="date"
                            value={dateStart}
                            onChange={e => setDateStart(e.target.value)}
                        />
                </Form.Group>

                <Form.Group className="col-6">
                    <Form.Label>To</Form.Label>
                        <Form.Control
                            type="date"
                            value={dateEnd}
                            onChange={e => setDateEnd(e.target.value)}
                        />
                </Form.Group>
                <hr/>
            </Form>
                <Container>
                    <div className="row">
                        <div className="col-md-6 col-sm-12">
                            {/* check first if date selected are not empty before displaying the text */}
                            {(dateStart !== "" && dateEnd !== "") ?
                                 <h3 className="text-center">Income Breakdown</h3>
                                :
                                null
                            }
                            <Pie data={dataIncome} />
                        </div>
                         <div className="col-md-6 col-sm-12">
                         {/* check first if date selected are not empty before displaying the text */}
                            {(dateStart !== "" && dateEnd !== "") ?
                                 <h3 className="text-center">Expense Breakdown</h3>
                                :
                                null
                            }
                            <Pie data={dataExpense} />
                         </div>
                    </div>
                </Container>
        </Container>
    )
}

// 33. Create Chart for Trend
