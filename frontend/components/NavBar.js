import React, { useContext } from 'react';
// Import nextJS Link component for client-side navigation
import Link from 'next/link';
// Import necessary bootstrap components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

    export default function NavBar(){
        // Consume the UserContext and destructure it to access the user state from the context provider
        const { user } = useContext(UserContext);

        return (
            <Navbar bg="dark" variant="dark" expand='lg'>
               
                { (user.id !== null)?
                    <React.Fragment> 
                        <Navbar.Brand href="/records">Budget Tracker</Navbar.Brand>
                           
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            
                                <Nav className="mr-auto">
                                    <Link href="/categories">
                                        <a className="nav-link" role="button">Categories</a>
                                    </Link>
                                
                                    <Link href="/records">
                                        <a className="nav-link" role="button">Records</a>
                                    </Link>
                                
                                    <Link href="/barExpense">
                                        <a className="nav-link" role="button">Monthly Expense</a>
                                    </Link>
                            
                                    <Link href="/barIncome">
                                        <a className="nav-link" role="button">Monthly Income</a>
                                    </Link>
                                
                                    <Link href="/trend">
                                        <a className="nav-link" role="button">Trend</a>
                                    </Link>
                            
                                    <Link href="/breakdown">
                                        <a className="nav-link" role="button">Breakdown</a>
                                    </Link>
                                </Nav>

                                <Nav className="ml-auto">
                                    <Link href="/logout">
                                        <a className="nav-link" role="button">Logout</a>
                                    </Link>
                                </Nav>
                         
                        </Navbar.Collapse>
                    </React.Fragment>
                :
               
                <Navbar.Brand href="/">Budget Tracker</Navbar.Brand>
                  
                }
            </Navbar>

            
        )

    }