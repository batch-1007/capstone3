import { useSate, useContext, useEffect, useState } from 'react';
import Router from 'next/router';
import { Form, Button, Card , Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import AppHelper from '../app-helper';
import View from '../components/View';
import Link from 'next/link';


export default function index() {

    return(  
        <View title={'Login'}>
            <Row className="justify-content-center">	
                <Col xs md="6">	
                    <h3>Login</h3>
                    <LoginForm />
                </Col>
            </Row>
        </View>
    )

}

const LoginForm = () => {

    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');;
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] =useState(false);

    function authenticate(e){
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }


        fetch(`${AppHelper.API_URL}/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data =>{
            //data is the object with key value of accessToken
            // console.log(data);

            if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)

                //redirect temporarily to index.js
                Router.push('/records')
            }else{
                if(data.error === 'does-not-exist'){
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password'){
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error'){
                    Swal.fire('Login Type Error', 'You may have registered throug a different login procedure, try alternative login procedure.', 'error')
                }
            }

        })

    };


    const authenticateGoogleToken = (response) => {
        console.log(response.tokenId)

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        }

        fetch(`${ AppHelper.API_URL }/users/verify-google-id-token`, options)
        .then(AppHelper.toJSON)
        .then(data => {

            console.log(data);

            if (typeof data.accessToken !== 'undefined'){

                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);
                //redirect to records
                Router.push('/records')
            } else {

                if (data.error === 'google-auth-error'){
                    Swal.fire(
                        'Google Auth Error', 
                        'Google authentication procedure failed.', 
                        'error'
                    )
                } else if (data.error === 'login-type-error') {
                    Swal.fire(
                        'Login Type Error', 
                        'You may have registered through a different login procedure.', 
                        'error'
                    )
                }

            }

        })

    };


    const retrieveUserDetails = (accessToken) => {

        const options = {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data);
            setUser({
                id: data._id
            })
        })

    }

    return(

        <Card>
            <Card.Header>Login Details</Card.Header>
            <Card.Body>
                <Form onSubmit={ authenticate }>

                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password"
                            placeholder="Password" 
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Button className="btn btn-block bg-primary mb-3" type="submit">
                            Login
                    </Button>

                    <GoogleLogin
                        clientId="10444938299-43406hruphuohmc78cqr4v6mlv01t4op.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />
                    
                    <p className="text-center mt-2">"If you have'nt registered yet,"<a role="button" href="/register">Sign Up Here</a></p>
                </Form>
            </Card.Body>
        </Card>
    )

}



