import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext'
import AppHelper from '../../app-helper'


import { Form } from 'react-bootstrap'
import {Button, Container, Card} from 'react-bootstrap'

import Swal from 'sweetalert2';

import View from '../../components/View';
import {Row, Col} from 'react-bootstrap';

export default function index(){
    return(  
        <View title={'New Record'}>
            <Row className="justify-content-center">	
                    < NewRecordPage />
            </Row>
        </View>
    )
}

const NewRecordPage = () => {

    const { user } = useContext(UserContext);

    const [categories, setCategories] = useState([]);

    const [categoryName, setCategoryName] = useState('');
    const [categoryType, setCategoryType] = useState('');
    const [amount, setAmount] = useState(0);
    const [description, setDescription] = useState('');


    function addRecord(e){
        e.preventDefault();


        // fetch('http://localhost:4000/api/users/records', {
        fetch('https://git.heroku.com/hidden-taiga-96614.git/api/users/records', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ AppHelper.getAccessToken() }` 
            },
            body: JSON.stringify({
                categoryType: categoryType,
                categoryName: categoryName,
                amount: amount,
                description: description
            })
        })
            .then(res => res.json())
            .then(data =>{
                Swal.fire('New record added', `${categoryType} successfully added`, 'success') 
            })

    }



       //CODE TO DISPLAY THE CATEGORY NAME VALUES WITH THERE RESPECTED CATEGORY TYPE
       useEffect(() =>{
            console.log(user.id)

            fetch('http://localhost:4000/api/users/details', {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${ AppHelper.getAccessToken() }` 
                }
            })
            .then(res => res.json())
            .then(data =>{
                console.log(data.categories)
                console.log(categoryType)

                if(categoryType === "Expense"){

                    const categoryList = data.categories.map(category =>{
                      
                        if(category.categoryType === "Expense"){
                              console.log(category.categoryName)
                              console.log(category._id)
                            return(
                                <option key={category._id}>{category.categoryName}</option>
                            )
                        }
                    })
                    setCategories(categoryList)
                }

                if(categoryType === "Income"){

                    const categoryList = data.categories.map(element =>{
                        // console.log(element.categoryName)
                        if(element.categoryType === "Income"){
                            return(
                                <option key={element.categoryName}>{element.categoryName}</option>
                            )
                        }
                    })
                    setCategories(categoryList)
                }
                   
            })
       }, [categoryType,user.records])
        

    
    return(
       <Container className="mt-5 pt-4 mb-5 ">
           <div className="justify-content-center row">
               <div className="col-md-6 col">
                    <h3>New Record</h3>
                    <Card>
                        <Card.Header>Record Information</Card.Header>
                        <Card.Body>
                            <Form onSubmit={addRecord}>
                            <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryType">Category Type:</Form.Label>
                                    <Form.Control as="select" required onChange={e => setCategoryType(e.target.value)} >
                                            <option default>Select category</option>
                                            <option value="Income">Income</option>
                                            <option value="Expense">Expense</option>
                                    </Form.Control>
                                </Form.Group>

                              
                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="categoryType">Category Name:</Form.Label>
                                    <Form.Control as="select" htmlFor="categoryName" onChange={e => setCategoryName(e.target.value)}>
                                           {categories}
                                    </Form.Control>
                                
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="amount">Amount:</Form.Label>
                                    <Form.Control
                                        type="number"
                                        placeholder="Enter amount"
                                        value={amount}
                                        onChange={e => setAmount(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label className="form-label" htmlFor="description">Description:</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter description"
                                        value={description}
                                        onChange={e => setDescription(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                             
                               
                                <Button className="btn btn-primary" type="submit">Submit</Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
            
        </Container> 
    )
}