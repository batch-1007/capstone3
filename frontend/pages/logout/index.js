import { useContext, useEffect } from 'react';
import Router from 'next/router';
import UserContext from '../../UserContext';
import View from '../../components/View';
import {Row, Col} from 'react-bootstrap'

export default function index() {

    return(  
        <View title={'Logout'}>
            <Row className="justify-content-center">	
                <Col xs md="6">	
                    <Logout />
                </Col>
            </Row>
        </View>
    )

}


const Logout = ()=>{
     
        // Consume the UserContext and destructure it to access the user and unsetUser values from the context provider
        const { unsetUser } = useContext(UserContext);
        
        // Invoke unsetUser only after initial render
        useEffect(() => {
            // Invoke unsetUser() to clear local storage of user info
            unsetUser();
            Router.push('/');
        },[])

        return null;
}
